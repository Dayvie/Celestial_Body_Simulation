# -*- coding: utf-8 -*-
"""
GUI Modul fuer die Planetensimulation

@author Christian Gubo, David Yesil, Jonathan Panchyrz, Joshua Hörmann, Timo Gruber
"""

from PyQt4 import QtCore, QtGui, uic
import collections, sys
from enum import Enum


class GalaxyGraphicalUserInterface (QtGui.QMainWindow):
    """
    The class which handles the elements of the Graphical User Interface
    """
    def __init__(self, logic_manager):
        super().__init__()
        self.logic_manager = logic_manager
        self.ui = uic.loadUi('galaxy.ui')
        self.widget = self.ui.galaxyWidget
        self.connect(self.ui.actionExit, QtCore.SIGNAL('triggered()'), self.close)
        self.connect(self.ui.start_pause, QtCore.SIGNAL('clicked()'), self.start_hit)
        self.connect(self.ui.stop, QtCore.SIGNAL('clicked()'), self.stop_simulation)
        self.connect(self.ui.zoom_slider, QtCore.SIGNAL('sliderReleased()'), self.zoom_changed)
        self.connect(self.ui.radius_slider, QtCore.SIGNAL('sliderReleased()'), self.readjust_radius)
        self.Point3D = collections.namedtuple('Point3D', ['x', 'y', 'z'])
        self.Sphere3D = collections.namedtuple('Mass3D', ['x', 'y', 'z', 'r'])
        self.widget.light_position = self.Point3D(1e10, 1e11, 1e11*2)
        self.STANDARD_CAMERA_POSITION = (1e10, 1e10, 1e12)
        self.widget.camera_position = self.Point3D(self.STANDARD_CAMERA_POSITION[0], self.STANDARD_CAMERA_POSITION[1],
                                                   self.STANDARD_CAMERA_POSITION[2])
        self.start_state_map = {SimulationState.running: self.pause_simulation,
                                SimulationState.pause: self.continue_simulation,
                                SimulationState.stop: self.make_new_simulation}
        self.central_object_map = {"Nichts": 0, "Sonne": 6960000000*4, "Schwarzesloch": 1}
        self.simulation_state = SimulationState.stop
        self.ui.show()
        self.widget.show()

    def zoom_changed(self):
        """
        @author David Yesil
        changes the Z-value of the galaxy-widget-camera
        """
        self.widget.camera_position = self.Point3D(self.STANDARD_CAMERA_POSITION[0],
                                                   self.STANDARD_CAMERA_POSITION[1],
                                                   self.STANDARD_CAMERA_POSITION[2]+self.ui.zoom_slider.value()*1e9)

    def readjust_radius(self):
        """
        @author David Yesil
        changes the radius of all celestial_bodies except of the central body based on the radius set in the GUI
        """
        scale_factor = 1e7*4
        celestial_body_manager = self.logic_manager.celestial_body_manager
        if celestial_body_manager is None:
            return
        for celestial_body in list(celestial_body_manager.celestial_bodies.values()):
            if celestial_body.name == "CENTRAL":
                continue
            celestial_body.radius = celestial_body_manager.RADIUS + self.ui.radius_slider.value() * scale_factor

    def continue_simulation(self):
        self.simulation_state = SimulationState.running

    def start_hit(self):
        """
        @author David Yesil
        maps the current state to a method. This method will only be called when someone hit the start button
        :return:
        """
        self.start_state_map[self.simulation_state]()

    def make_new_simulation(self):
        """
        @author David Yesil
        Does operations for starting a new simulation
        """
        self.logic_manager.start_new_simulation(
                (self.ui.mass_min.value(), self.ui.mass_max.value()),
                self.ui.planet_count.value(), self.central_object_map[self.ui.central_object.currentText()],
                self.ui.mass_central.value(), self.ui.delta_time.value(), space=self.ui.space.value()*1e11)
        self.widget.start_animation_simulation(self.logic_manager.celestial_body_manager.celestial_bodies)
        self.simulation_state = SimulationState.running
        self.readjust_radius()

    def pause_simulation(self):
        self.simulation_state = SimulationState.pause

    def stop_simulation(self):
        self.logic_manager.celestial_body_manager = None
        self.simulation_state = SimulationState.stop

    def close(self):
        sys.exit(0)


class SimulationState(Enum):
    """
    @author David Yesil
    class which helps realizing the GUI which state the logic is in
    """
    running = 1
    stop = 2
    pause = 3
