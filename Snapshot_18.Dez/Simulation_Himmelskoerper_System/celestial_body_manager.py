"""
Created on Sun Oct 18 16:50:51 2015

@author: David Yesil, Christian Gubo, Jonathan Panchyrz, Joshua Hörmann, Timo Gruber
"""


from abc import ABCMeta
import copy
import numpy as np
from numba import jit


global G
G = 6.67428e-11


class CelestialBody(object):
    """
    Classes derived from this one represent CelestialBodies in this simulation
    """
    __metaclass__ = ABCMeta

    def __init__(self, name, pos_np, mass, start_velocity_np, radius, celestial_body_manager):
        """
        @author David Yesil
        :param name: the name of this body
        :param initial_distance: the initial y value of this body
        :param mass: the mass of this body in kg
        :param start_velocity_np: the initial velocity of this body in m/s
        :param radius: the radius of this body in m
        :param celestial_body_manager: A reference to the class which holds the other celestial bodies
        """
        self.pos_vector = pos_np
        self.mass = mass
        self.name = name
        self.tangential_vector = start_velocity_np
        self.celestial_body_manager = celestial_body_manager
        self.radius = radius

    def update(self, copy_list):
        """
        @author David Yesil
        Updates the position of a celestial body
        :param copy_list: A list of unupdated celestial bodies
        """
        self.apply_tangential_vector()
        gravitational_acceleration_vector = np.array([0, 0, 0], dtype='f')
        for celestial_body in copy_list:
            if celestial_body.name == self.name:
                continue
            gravitational_acceleration_vector =\
                gravitational_acceleration_vector + self.calculate_gravitational_acceleration_vector(celestial_body)
        self.calculate_positional_vector_after_gravitational_acceleration_vector(gravitational_acceleration_vector)
        self.update_tangential_vector(gravitational_acceleration_vector)

    def distance_celestial_body(self, celestial_body):
        """
        :return: the distance between two planets
        """
        return np.linalg.norm(self.pos_vector - celestial_body.pos_vector)

    def apply_tangential_vector(self):
        """
        @author David Yesil
        Applies the tangential vector which was calculated in the last update cycle
        """
        self.pos_vector = self.tangential_vector + self.pos_vector

    def calculate_gravitational_acceleration_vector(self, celestial_body):
        """
        @author David Yesil
        Calculates the gravitational force (vector) which this celestial body has towards the parameter body
        :param celestial_body: the celestial body this body is pulled towards
        :return: gravitational_acceleration_vector (np.array)
        """
        gravitational_acceleration_vector = \
            -(((self.mass * celestial_body.mass)/(self.distance_celestial_body(celestial_body)**3))* G *
              (self.pos_vector - celestial_body.pos_vector))/ self.mass
        return gravitational_acceleration_vector

    def calculate_positional_vector_after_gravitational_acceleration_vector(self, gravitational_acceleration_vector):
        """
        @author David Yesil
        Calculates the position of this celestial body after the summation of the gravitational forces
        """
        self.pos_vector = \
            (self.pos_vector + self.celestial_body_manager.delta_time * self.tangential_vector + 0.5 *
             gravitational_acceleration_vector * self.celestial_body_manager.delta_time ** 2)

    def update_tangential_vector(self, gravitational_acceleration_vector):
        """
        @author David Yesil
        :param gravitational_acceleration_vector:
        Readjusts the tangential vector after the gravitational force has been applied
        """
        self.tangential_vector = \
            gravitational_acceleration_vector * self.celestial_body_manager.delta_time + self.tangential_vector


class Planet(CelestialBody):
    def __init__(self, name, initial_distance, mass, start_velocity_np, radius, celestial_body_manager):
        super().__init__(name, initial_distance, mass, start_velocity_np, radius, celestial_body_manager)


class CelestialBodyManager(object):
    """
    @author David Yesil
    The class which holds the celestial_bodies and handles the update cycle
    """
    def __init__(
            self, mass_interval, planet_count, central_object_visible_radius, mass_central_object, dt_gui,
            space=1.496 * 10 ** 11 * 3):
        """
        Werte für unser Sonnensystem:
        mercury = Planet("Merkur", 57910000000, 328.5 * 10**21, 47360, 2440000, self)
        venus = Planet("Venus", 108200000000, 4.869 * 10 ** 24, 35020, 6052000, self)
        earth2 = Planet("Erde", 149600000000, 5.974 * 10**24, 29780, 6960000000, self)
        earth = Planet("Erde", 149600000000, 5.974 * 10**24, 29780, 6371000, self)
        mars = Planet("Mars", 227900000000, 3.301*10**23, 24130, 3390000, self)
        jupyter = Planet("Jupiter", 778500000000, 1.898 * 10**27, 13070, 69911000, self)
        saturn = Planet("Saturn", 1433000000000, 568.3 * 10 ** 24, 9690, 58232000, self)
        uranus = Planet("Uranus", 2877000000000, 86.81 * 10**24, 6810, 25362000, self)
        neptun = Planet("Neptun", 4498000000000, 102.4 * 10**24, 5430, 24622000, self)
        sun = Planet("Sonne", 0, 1.989 * 10**30, 0, 696000000, self)
        """
        self.system_impulse = None
        self.mass_central = mass_central_object*10e31
        self.space_division = self.make_space_division(space)
        self.RADIUS = 14 * 10e8
        self.celestial_bodies = {}
        self.list_celestial_bodies = self.generate_random_celestial_bodies(
            planet_count, mass_interval, central_object_visible_radius, self.RADIUS, self.space_division)
        for celestial_body in self.list_celestial_bodies:
            celestial_body.tangential_vector = self.calculate_starting_speed_celestial_body_vector(celestial_body)
            self.celestial_bodies[celestial_body.name] = celestial_body
        self.delta_time = dt_gui
        self.system_impulse = self.calculate_systemimpulse(self.list_celestial_bodies)
        
    def make_space_division(self, space):
        
        Sector1 = ((-space, 0), (-space, 0), (space/3, 0))
        Sector2 = ((0, space), (0, space), (-space/3, 0))
        Sector3 = ((-space, 0), (-space, 0), (-space/3, 0))
        Sector4 = ((0, space), (0, space), (0, space/3))
        Sector5 = ((-space, 0), (0, space), (-space/3, 0))
        Sector6 = ((0, space), (-space, 0), (0, space/3))
        Sector7 = ((-space, 0), (0, space), (0, space/3))
        Sector8 = ((0, space), (-space, 0), (-space/3, 0))

        space_division = {'Quadrant1': Sector1, 'Quadrant2': Sector2, 'Quadrant3': Sector3, 'Quadrant4': Sector4, 'Quadrant5': Sector5, 'Quadrant6': Sector6, 'Quadrant7': Sector7, 'Quadrant8': Sector8}
        return space_division

    def generate_random_position(self, random_interval):
        """
        @author David Yesil
        :param random_interval: the interval in which the positions will be calculated
        :return: a random generated 3D position as a np.array
        """
        return np.array([np.random.uniform(random_interval[0][0], random_interval[0][1]),
                        np.random.uniform(random_interval[1][0], random_interval[1][1]),
                        np.random.uniform(random_interval[2][0], random_interval[2][1])], dtype=np.float64)

    def generate_random_celestial_bodies(self, number_bodies, mass_interval, central_object_visible_radius, radius, space_division):
        """
        @author David Yesil
        :param number_bodies: the number of random celestial_bodies generated
        :param mass_interval: the interval for which the random mass will be chosen
        :param central_object_visible_radius: boolean which says if a big mass is in the centre of the simulation
        :param radius: the radius for the bodies
        :return: a list with celestial bodies
        """
        """
        celestial_bodies = {}
        for i in range(0, number_bodies):
            mass = self.generate_random_mass(mass_interval)
            celestial_bodies[i] = \
                Planet(str(i), self.generate_random_position((-149600000000*3, 149600000000*3)),
                mass, np.array([35020, 0, 0], dtype=np.float64), radius, self)
        if has_black_hole:
            name = "SUPERMASSIV"
            celestial_bodies[name] = Planet(name, np.array([0, 0, 0], dtype=np.float64),
                                    1.989 * 10**31, np.array([0, 0, 0], dtype=np.float64), radius*2, self)
        """
        celestial_bodies = []
        j = 1
        for i in range(0, number_bodies):
            mass = self.generate_random_mass(mass_interval)*10**18
            celestial_bodies.append(Planet(str(i), self.generate_random_position(self.space_division['Quadrant' + str(j)]),
                                           mass, np.array([0, 0, 0], dtype=np.float64), radius, self))
            j += 1
            if j == 9:
                j = 1
        if central_object_visible_radius:
            name = "CENTRAL"
            name2 = "PANCHY RULEZ"
            celestial_bodies.append(Planet(name, np.array([0, 0, 0], dtype=np.float64),
                                    self.mass_central, np.array([0, 0, 0], dtype=np.float64),
                                           central_object_visible_radius, self))
#            celestial_bodies.append(Planet(name, np.array([-1.496*10**11, 0, 0], dtype=np.float64),
#                                    1.989 * 10**33, np.array([0, 0, 0], dtype=np.float64), radius*2, self))
#            celestial_bodies.append(Planet(name2, np.array([1.496*10**11, 0, 0], dtype=np.float64),
#                                    1.989 * 10**33, np.array([0, 0, 0], dtype=np.float64), radius*2, self))
        return celestial_bodies

    def generate_random_mass(self, mass_interval):
        """
        @author David Yesil
        generates a random float in the interval tuple mass_interval
        :param mass_interval:
        :return:
        """
        return np.random.uniform(mass_interval[0], mass_interval[1])
        
    def calculate_systemimpulse(self, copy_list):
        impulse = 0
        for celestial_body in copy_list:
            impulse = impulse + celestial_body.mass * np.linalg.norm(celestial_body.tangential_vector)
        return impulse
        
    def calculate_total_mass(self):
        total_mass = 0
        for celestial_body in self.list_celestial_bodies:
            total_mass = total_mass + celestial_body.mass
        return total_mass

    def centre_of_gravity_without_celestial_body(self, planet):
        for celestial_body in self.list_celestial_bodies:
            if celestial_body.name == planet.name:
                continue
            else:
                mass_pos_product = celestial_body.mass * celestial_body.pos_vector
        
        return(mass_pos_product/self.calculate_total_mass())
        
    def calculate_starting_speed_celestial_body(self, celestial_body):
        radius = celestial_body.pos_vector - self.centre_of_gravity_without_celestial_body(celestial_body)
        radius_sum = np.linalg.norm(radius)
        total_mass = self.calculate_total_mass()
        x_numpy = (((total_mass - celestial_body.mass)/total_mass) * ((G * total_mass)/radius_sum)**0.5)
        return x_numpy
        
    def calculate_starting_speed_celestial_body_vector(self, celestial_body):
        z_vector = np.array([0, 0, 1], dtype=np.float64)
        numerator = np.cross((
            celestial_body.pos_vector - self.centre_of_gravity_without_celestial_body(celestial_body)), z_vector)
        denominator = np.linalg.norm(numerator)
        result = numerator / denominator
        return (self.calculate_starting_speed_celestial_body(celestial_body) * result)

    @jit
    def update_all(self):
        """
        @author David Yesil
        Updates all Celestial bodies held in the dict celestial_bodies and sets the system_impulse afterwards
        """
        copied_celestial_body_list = copy.deepcopy(list(self.celestial_bodies.values()))
        for current_celestial_body in copied_celestial_body_list:
            copied_celestial_body = copy.deepcopy(current_celestial_body)
            copied_celestial_body.update(copied_celestial_body_list)
            self.celestial_bodies[copied_celestial_body.name] = copied_celestial_body
