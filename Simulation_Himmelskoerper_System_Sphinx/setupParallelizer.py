# -*- coding: utf-8 -*-
"""
Created on Mon Jan  4 17:07:51 2016

@author: user
"""

from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

ext_modules=[ Extension("parallelizer", ["parallelizer.pyx"],
        extra_compile_args=['-O3'], libraries=['m']) ]
setup( name = 'Parallelizer',
  cmdclass = {'build_ext': build_ext},
  ext_modules = ext_modules)