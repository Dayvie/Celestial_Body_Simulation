Auto Generated Documentation
============================

celestial_body_manager
-----------------------

.. automodule:: celestial_body_manager
   :members:

gui_manager
------------

.. automodule:: gui_manager
   :members:

logic_manager
--------------

.. automodule:: logic_manager
   :members:

parallelizer
-------------

.. automodule:: logic_manager
   :members:

ParallelizerMain
-----------------

.. automudle:: ParallelizerMain
   :members:

setupCelestialBodyManager
--------------------------

.. automodule:: setupCelestialBodyManager
   :members:

setupParallelizer
------------------

.. automodule:: setupParallelizer
   :members:

tests
------

.. automodule:: tests
   :members:

