# -*- coding: utf-8 -*-
#pylint: disable=maybe-no-member
#pylint: disable=E1102
"""
@author: David Yesil
"""
import copy
import multiprocessing
from multiprocessing.managers import BaseManager

PORT = 53339


def start_host():
    """
    starts the server-process
    :return:
    """
    server = multiprocessing.Process(target=start_base_manager,
                                     daemon=True)
    server.start()


def start_base_manager():
    """
    starts the task_manager for parallelization
    :return:
    """
    task_queue = multiprocessing.JoinableQueue()
    result_queue = multiprocessing.Queue()
    TaskManager.register('get_job_queue',
                         callable=lambda: task_queue)
    TaskManager.register('get_result_queue',
                         callable=lambda: result_queue)
    task_manager = TaskManager(address=('', PORT), authkey=b'secret')
    print('starting queue server, socket', PORT)
    task_manager.get_server().serve_forever()


def create_update_jobs(copied_celestial_body_tuple, workload=15, manager=None):
    """
    :param workload: creates jobs for each worker
    """
    if manager is None:
        manager = connect_to_manager()
    job_queue, result_queue = manager.get_job_queue(), manager.get_result_queue()
    lower_border = 0
    upper_border = workload
    while True:
        job_queue.put((lower_border, upper_border,
                       copied_celestial_body_tuple))
        if upper_border > len(copied_celestial_body_tuple):
            break
        lower_border += workload
        upper_border += workload
    job_queue.join()
    return result_queue


def celestial_body_update_worker(in_queue, out_queue):
    """
    does the updateroutrine for the celestial_bodies
    :param in_queue:
    :param out_queue:
    :return:
    """
    while True:
        arg_tuple = in_queue.get()
        tasks = (arg_tuple[2])[arg_tuple[0]:arg_tuple[1]]
        out_list = []
        for celestial_body in tasks:
            celestial_body_copy = copy.deepcopy(celestial_body)
            celestial_body_copy.update(arg_tuple[2])
            out_list.append(celestial_body_copy)
        out_queue.put(tuple(out_list))
        in_queue.task_done()


def start_processes(processes):
    """
    starts all processes in the given list
    """
    for process in processes:
        process.start()


def terminate_processes(processes):
    """
    terminates all processes in the given list
    """
    for process in processes:
        process.terminate()


def start_workers(server_ip='localhost', daemon=True):
    """
    starts the processes which update the celestial_bodies
    :param server_ip:
    :param daemon:
    :return:
    """
    manager = connect_to_manager(server_ip=server_ip)
    job_queue = manager.get_job_queue()
    result_queue = manager.get_result_queue()
    nr_of_processes = multiprocessing.cpu_count()
    worker_processes = [multiprocessing.Process(
        target=celestial_body_update_worker,
        args=(job_queue, result_queue), daemon=daemon)
                        for i in range(nr_of_processes)]
    start_processes(worker_processes)
    print("all workers connected and started")


def connect_to_manager(server_ip='localhost'):
    """
    connects to the basemanager
    :return: the basemanager
    """
    TaskManager.register('get_job_queue')
    TaskManager.register('get_result_queue')
    manager = TaskManager(address=(server_ip, PORT), authkey=b'secret')
    manager.connect()
    return manager


class TaskManager(BaseManager):
    """
    derivation for using the BaseManager API Class
    """
    pass

if __name__ == "__main__":
    from sys import argv
    if len(argv) == 1:
        start_workers(daemon=False)
    elif len(argv) == 2:
        start_workers(server_ip=argv[1], daemon=False)
    else:
        print('usage:', argv[0], 'server_IP server_socket')
        exit(0)
