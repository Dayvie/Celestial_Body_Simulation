# -*- coding: utf-8 -*-
#pylint: disable=maybe-no-member
"""
@author: Christian Gubo, David Yesil, Jonathan Panchyrz, Joshua Hörmann,
    Timo Gruber
"""
from gui_manager import GalaxyGraphicalUserInterface, SimulationState
from celestial_body_manager import CelestialBodyManager
from PyQt4 import QtCore, QtGui
import parallelizer, sys

class LogicManager(object):
    """
    The main manager class which handles Gui and Logic
    """
    def __init__(self, app):
        parallelizer.start_host()
        self.app = app
        self.logic_timer = QtCore.QTimer()
        self.logic_update_interval = 100/3
        self.gui = GalaxyGraphicalUserInterface(self)
        self.celestial_body_manager = None
        self.gui.connect(self.logic_timer, QtCore.SIGNAL('timeout()'),
                         self.update)
        self.logic_timer.setInterval(self.logic_update_interval)
        parallelizer.start_workers()
        self.logic_timer.start()

    def update(self):
        """
        @author David Yesil
        updates the entire logic
        """
        if self.gui.simulation_state == SimulationState.running \
            and self.celestial_body_manager is not None:
            self.celestial_body_manager.update_all()

    def start_new_simulation(self, mass_interval, planet_count,
                             central_object_visible_radius,
                             mass_central_object,
                             dt_gui, space=1.496 * 10 ** 11 * 3):
        """
        @author David Yesil
        Factory method which builds a new celestial_body_manager
        :param mass_interval: interval in which bodies will be randomly
        generated
        :param planet_count: count of planets in the simulation
        :param central_object_visible_radius: radius of the central object
        :param mass_central_object: mass of the central object
        :param dt_gui: times which passes each update-cycle
        :param space: space between bodies which are randomly generated
        """
        self.celestial_body_manager = CelestialBodyManager((mass_interval[0], mass_interval[1]),
                                                           planet_count,
                                                           central_object_visible_radius,
                                                           mass_central_object, dt_gui, space)
        self.gui.qt_interface.impulse.setText(
            str(self.celestial_body_manager.system_impulse))

if __name__ == "__main__":
    sys.path.append("widgets")
    app = QtGui.QApplication(sys.argv)
    logic_manager = LogicManager(app)
    sys.exit(app.exec_())
