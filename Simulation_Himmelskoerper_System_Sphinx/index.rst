.. Simulation_Himmelskoerper_System documentation master file, created by
   sphinx-quickstart on Mon Jan 11 01:48:30 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Simulation_Himmelskoerper_System's documentation!
============================================================

Contents:

.. toctree::
   :maxdepth: 2

   problems
   code

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

