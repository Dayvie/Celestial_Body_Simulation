# -*- coding: utf-8 -*-
"""
Created on Sat Jan  2 12:12:34 2016

@author: user
"""
from parallelizer import start_workers
from sys import argv
if len(argv) == 1:
    start_workers(daemon=False)
elif len(argv) == 2:
    start_workers(server_ip=argv[1], daemon=False)
else:
    print('usage:', argv[0], 'server_IP server_socket')
    exit(0)
