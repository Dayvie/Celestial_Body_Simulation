"""
Created on Sun Oct 18 16:50:51 2015

@author: David Yesil, Christian Gubo, Jonathan Panchyrz, Joshua Hörmann, Timo Gruber
"""

import parallelizer as pa
import copy, time
import numpy as np
import multiprocessing


G = 6.67428e-11
np_datatype = np.float32


class CelestialBody(object):
    """
    Classes derived from this one represent CelestialBodies in this simulation
    """
    def __init__(self, name, pos_np, mass, start_velocity_np, delta_time):
        """
        @author David Yesil
        :param name: the name of this body
        :param initial_distance: the initial y value of this body
        :param mass: the mass of this body in kg
        :param start_velocity_np: the initial velocity of this body in m/s
        :param radius: the radius of this body in m
        :param celestial_body_manager: A reference to the class which holds the other celestial bodies
        """
        self.pos_vector = pos_np
        self.mass = mass
        self.name = name
        self.tangential_vector = start_velocity_np
        self.delta_time = delta_time

    def update(self, copy_list):
        """
        @author David Yesil
        Updates the position of a celestial body
        :param copy_list: A list of unupdated celestial bodies
        """
        self.apply_tangential_vector()
        gravitational_acceleration_vector = np.array([0, 0, 0], dtype=np_datatype)
        for celestial_body in copy_list:
            if celestial_body.name == self.name:
                continue
            gravitational_acceleration_vector +=\
                self.calculate_gravitational_acceleration_vector(celestial_body)
        self.calculate_positional_vector_after_gravitational_acceleration_vector(
                gravitational_acceleration_vector)
        self.update_tangential_vector(gravitational_acceleration_vector)

    def distance_celestial_body(self, celestial_body):
        """
        :return: the distance between two planets
        """
        return np.linalg.norm(self.pos_vector - celestial_body.pos_vector)

    def apply_tangential_vector(self):
        """
        @author David Yesil
        Applies the tangential vector which was calculated in the last update cycle
        """
        self.pos_vector = self.tangential_vector + self.pos_vector

    def calculate_gravitational_acceleration_vector(self, celestial_body):
        """
        @author David Yesil
        Calculates the gravitational force (vector) which this celestial body has towards the parameter body
        :param celestial_body: the celestial body this body is pulled towards
        :return: gravitational_acceleration_vector (np.array)
        """
        gravitational_acceleration_vector = \
            -(((self.mass * celestial_body.mass)/(self.distance_celestial_body(celestial_body)**3))* G *
              (self.pos_vector - celestial_body.pos_vector))/ self.mass
        return gravitational_acceleration_vector

    def calculate_positional_vector_after_gravitational_acceleration_vector(self, gravitational_acceleration_vector):
        """
        @author David Yesil
        Calculates the position of this celestial body after the summation of the gravitational forces
        """
        self.pos_vector = \
            (self.pos_vector + self.delta_time * self.tangential_vector + 0.5 *
             gravitational_acceleration_vector * self.delta_time ** 2)

    def update_tangential_vector(self, gravitational_acceleration_vector):
        """
        @author David Yesil
        :param gravitational_acceleration_vector:
        Readjusts the tangential vector after the gravitational force has been applied
        """
        self.tangential_vector = \
            gravitational_acceleration_vector * self.delta_time + self.tangential_vector


class CelestialBodyManager(object):
    """
    @author David Yesil
    The class which holds the celestial_bodies and handles the update cycle
    """
    def __init__(
            self, mass_interval, planet_count, mass_central_object, dt_gui,
            space=1.496 * 10 ** 11 * 3):
        self.delta_time = dt_gui
        self.planet_count = planet_count
        self.cores = multiprocessing.cpu_count()
        self.system_impulse = None
        self.mass_central = mass_central_object*10e31
        self.space_division = self.make_space_division(space)
        self.RADIUS = 14 * 10e8
        self.celestial_bodies = {}
        self.list_celestial_bodies = self.generate_random_celestial_bodies(
            planet_count, mass_interval, self.space_division)
        for celestial_body in self.list_celestial_bodies:
            celestial_body.tangential_vector = self.calculate_starting_speed_celestial_body_vector(celestial_body)
            self.celestial_bodies[celestial_body.name] = celestial_body
        self.system_impulse = self.calculate_systemimpulse(self.list_celestial_bodies)
        self.list_celestial_bodies = None
        
    def make_space_division(self, space):
        Sector1 = ((-space, 0), (-space, 0), (space/3, 0))
        Sector2 = ((0, space), (0, space), (-space/3, 0))
        Sector3 = ((-space, 0), (-space, 0), (-space/3, 0))
        Sector4 = ((0, space), (0, space), (0, space/3))
        Sector5 = ((-space, 0), (0, space), (-space/3, 0))
        Sector6 = ((0, space), (-space, 0), (0, space/3))
        Sector7 = ((-space, 0), (0, space), (0, space/3))
        Sector8 = ((0, space), (-space, 0), (-space/3, 0))
        space_division = {'Quadrant1': Sector1, 'Quadrant2': Sector2, 'Quadrant3': Sector3, 'Quadrant4': Sector4, 'Quadrant5': Sector5, 'Quadrant6': Sector6, 'Quadrant7': Sector7, 'Quadrant8': Sector8}
        return space_division

    def generate_random_position(self, random_interval):
        """
        @author David Yesil
        :param random_interval: the interval in which the positions will be calculated
        :return: a random generated 3D position as a np.array
        """
        return np.array([np.random.uniform(random_interval[0][0], random_interval[0][1]),
                        np.random.uniform(random_interval[1][0], random_interval[1][1]),
                        np.random.uniform(random_interval[2][0], random_interval[2][1])],
                        dtype=np_datatype)

    def generate_random_celestial_bodies(self, number_bodies, mass_interval, space_division):
        """
        @author David Yesil
        :param number_bodies: the number of random celestial_bodies generated
        :param mass_interval: the interval for which the random mass will be chosen
        :param central_object_visible_radius: boolean which says if a big mass is in the centre of the simulation
        :param radius: the radius for the bodies
        :return: a list with celestial bodies
        """
        celestial_bodies = []
        j = 1
        for i in range(0, number_bodies):
            mass = self.generate_random_mass(mass_interval)*10**18
            celestial_bodies.append(CelestialBody(str(i), self.generate_random_position(
                    self.space_division['Quadrant' + str(j)]),
                                           mass, np.array([0, 0, 0], dtype=np_datatype), self.delta_time))
            j += 1
            if j == 9:
                j = 1
        name = "CENTRAL"
        celestial_bodies.append(CelestialBody(name, np.array([0, 0, 0], dtype=np_datatype),
                                    self.mass_central, np.array([0, 0, 0], dtype=np_datatype),
                                           self.delta_time))
        return celestial_bodies

    def generate_random_mass(self, mass_interval):
        """
        @author David Yesil
        generates a random float in the interval tuple mass_interval
        :param mass_interval:
        :return:
        """
        return np.random.uniform(mass_interval[0], mass_interval[1])
        
    def calculate_systemimpulse(self, copy_list):
        impulse = 0
        for celestial_body in copy_list:
            impulse = impulse + celestial_body.mass * np.linalg.norm(celestial_body.tangential_vector)
        return impulse
        
    def calculate_total_mass(self):
        total_mass = 0
        for celestial_body in self.list_celestial_bodies:
            total_mass = total_mass + celestial_body.mass
        return total_mass

    def centre_of_gravity_without_celestial_body(self, planet):
        for celestial_body in self.list_celestial_bodies:
            if celestial_body.name == planet.name:
                continue
            else:
                mass_pos_product = celestial_body.mass * celestial_body.pos_vector
        
        return(mass_pos_product/self.calculate_total_mass())
        
    def calculate_starting_speed_celestial_body(self, celestial_body):
        radius = celestial_body.pos_vector - self.centre_of_gravity_without_celestial_body(celestial_body)
        radius_sum = np.linalg.norm(radius)
        total_mass = self.calculate_total_mass()
        x_numpy = (((total_mass - celestial_body.mass)/total_mass) * ((G * total_mass)/radius_sum)**0.5)
        return x_numpy
        
    def calculate_starting_speed_celestial_body_vector(self, celestial_body):
        z_vector = np.array([0, 0, 1], dtype=np_datatype)
        numerator = np.cross((
            celestial_body.pos_vector - self.centre_of_gravity_without_celestial_body(celestial_body)), z_vector)
        denominator = np.linalg.norm(numerator)
        result = numerator / denominator
        return (self.calculate_starting_speed_celestial_body(celestial_body) * result)

    def update_all(self):
        """
        @author David Yesil
        Updates all Celestial bodies held in the dict celestial_bodies
        """
        result_queue = pa.create_update_jobs(tuple(self.celestial_bodies.values()),
                                             workload=15)
        self.overwrite_celestial_bodies(result_queue)

    def overwrite_celestial_bodies(self, out_queue):
        while not out_queue.empty():
            out_tuple = out_queue.get()
            for updated_celestial_body in out_tuple:
                self.celestial_bodies[updated_celestial_body.name] = updated_celestial_body
